package org.academiadecodigo.stringrays.moneyidolexchanger2;
import org.academiadecodigo.simplegraphics.pictures.Picture;



public class Main {

    public static void main(String[] args) throws InterruptedException {

        Launcher l = new Launcher();

        l.start();

    }


    private static class Launcher {

        private Game game = new Game();
        //private Rectangle rectangle = new Rectangle(10, 10, 350, 600);
        private Picture pic = new Picture(35, 201, "resources/images/Lisa_portrait.png");
        private Picture picBG = new Picture(10, 10, "resources/images/gameBG.jpg");


        public void start() throws InterruptedException {


            picBG.draw();
            pic.draw();

            Thread.sleep(4000);

            pic.delete();
            picBG.delete();

            game.init();
            game.start();


        }

    }
}


