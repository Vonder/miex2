package org.academiadecodigo.stringrays.moneyidolexchanger2;

import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Coins.Coin;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Grid;

public class CoinGrid {

    /**
     * {1, 2, 1, 2, 1},
     * {2, 1, 2, 1, 2},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * {0, 0, 0, 0, 0},
     * <p>
     * game board
     * cols = grid.cols
     * rows = grid.rows -2
     */

    private Coin[][] coinGrid;
    private boolean gridPattern;
    private int coinToBeResolvedCounter = 0;
    private boolean gameOver;

    private int col;
    private int row;

    public CoinGrid(int col, int row) {

        this.col = col;
        this.row = row;

        coinGrid = new Coin[row - 2][col];

    }

    /**
     * @return returns array with current game state
     */
    public void fillCoinGrid(Grid grid) {

        for (int i = 0; i < coinGrid.length; i++) {

            for (int j = 0; j < coinGrid[i].length; j++) {
                coinGrid[i][j] = new Coin(Coin.CoinType.NULLCOIN, grid.makeGridPosition(j, i));
            }
        }

    }


    /**
     * create new values on row 0;
     */
    public void spawnCoins() {

        if (gridPattern) {
            for (int i = 0; i < coinGrid[0].length; i++) {

                if (i % 2 == 0) {


                    coinGrid[0][i].setType(Coin.CoinType.ONE);
                } else
                    coinGrid[0][i].setType(Coin.CoinType.FIVE);

            }

            gridPattern = !gridPattern;
            return;

        }

        for (int i = 0; i < coinGrid[0].length; i++) {

            if (i % 2 == 0) {


                coinGrid[0][i].setType(Coin.CoinType.FIVE);
            } else
                coinGrid[0][i].setType(Coin.CoinType.ONE);

        }

        gridPattern = !gridPattern;

    }


    /**
     * @param col col where values shift down
     */

    public void shiftCol(int col) {

        for (int i = coinGrid.length - 1; i > 0; i--) {
            coinGrid[i][col].setType(coinGrid[i - 1][col].getCoinType());
        }

        coinGrid[0][col].setType(Coin.CoinType.NULLCOIN);
    }


    /**
     * push all values down, preserving positions. if trying to update values into grid.maxHeight, Game over.
     * should result in first row cleared.
     *
     * @return return true if game over;
     */
    public void descendCoins() {
        for (int i = 0; i < coinGrid.length; i++) {
           if (coinGrid[coinGrid.length - 1][0].getCoinType() != Coin.CoinType.NULLCOIN) {
               gameOver = true;
           }
        }
        for (int i = 0; i < coinGrid[i].length; i++) {
            shiftCol(i);
        }
    }

    public boolean checkGameOver() {
        return gameOver;
    }

    public void pushCoinsOnCol(int col, Coin.CoinType type, int quantity) {

        for (int i = coinGrid.length - 1; i >= 0; i--) {
            if (coinGrid[i][col].getCoinType() != Coin.CoinType.NULLCOIN ||
                    i == 0 && coinGrid[i][col].getCoinType() == Coin.CoinType.NULLCOIN) {

                if (i == 0 && coinGrid[i][col].getCoinType() == Coin.CoinType.NULLCOIN) {
                    coinGrid[i][col].setType(type);
                } else {
                    i++;
                    coinGrid[i][col].setType(type);
                }
                if (quantity > 1) {
                    pushCoinsOnCol(col, type, --quantity);
                    return;
                }
                if (quantity == 1) {
                    checkNeighbour(i, col);
                    resolve(type, col);
                }
                return;
            }
        }
    }


    public void resolve(Coin.CoinType resolvingType, int col) {

        if (resolvingType == Coin.CoinType.NULLCOIN) {
            return;
        }

        // TODO: 12/10/2019 Stop floating coins after resolving
        // todo Give Better resolving feedback example more time before erasing coins

        int local = (int) Math.floor(coinToBeResolvedCounter / resolvingType.getDivider());
        System.out.println("number of coins = " + coinToBeResolvedCounter);
        System.out.println("local = " + local);
        coinToBeResolvedCounter = 0;
        if (local > 0) {
            for (int i = 0; i < coinGrid.length; i++) {
                for (int j = 0; j < coinGrid[i].length; j++) {
                    if (coinGrid[i][j].getVisited()) {
                        coinGrid[i][j].setUnvisited();
                        coinGrid[i][j].setType(Coin.CoinType.NULLCOIN);
                    }
                }
            }
            pushCoinsOnCol(col, Coin.getNextValue(resolvingType), local);
        }
        clearVisited();
    }


    public void clearVisited() {
        for (int i = 0; i < coinGrid.length; i++) {
            for (int j = 0; j < coinGrid[i].length; j++) {
                coinGrid[i][j].setUnvisited();
            }
        }
    }


    public Coin.CoinType getTypeOfFirstCoin(int col) {


        for (int i = coinGrid.length - 1; i >= 0; i--) {
            if (coinGrid[i][col].getCoinType() != Coin.CoinType.NULLCOIN) {

                return coinGrid[i][col].getCoinType();
            }
        }

        return Coin.CoinType.NULLCOIN;
    }


    public int getAllCoinsOfType(int col, Coin.CoinType type) {

        int numOfType = 0;

        for (int i = coinGrid.length - 1; i >= 0; i--) {

            if (coinGrid[i][col].getCoinType() == Coin.CoinType.NULLCOIN) {
                continue;
            }

            if (coinGrid[i][col].getCoinType() != type) {
                System.out.println(col + " " + type + " " + coinGrid[i][col].getCoinType());
                return numOfType;
            }

            coinGrid[i][col].setType(Coin.CoinType.NULLCOIN);
            numOfType++;

        }

        return numOfType;
    }




    /**
     * pick position in grid.
     * process position and all neighbouring positions with the same type.
     */
    public void checkNeighbour(int x, int y) {

        System.out.println("x" + x + ":y" + y + " has -> " + coinGrid[x][y].getCoinType());

        coinGrid[x][y].setVisited();
        coinToBeResolvedCounter++;

        if (coinGrid[x][y].getCoinType() == Coin.CoinType.NULLCOIN) {
            return;
        }

        if (y - 1 >= 0) { //call Left
            if (coinGrid[x][y].getCoinType() == coinGrid[x][y - 1].getCoinType() && !coinGrid[x][y - 1].getVisited()) {
                checkNeighbour(x, y - 1);
            }
        }
        if (x - 1 >= 0) { //call Up
            if (coinGrid[x][y].getCoinType() == coinGrid[x - 1][y].getCoinType() && !coinGrid[x - 1][y].getVisited()) {
                checkNeighbour(x - 1, y);
            }
        }
        if (y + 1 < coinGrid[x].length) { //call Right
            if (coinGrid[x][y].getCoinType() == coinGrid[x][y + 1].getCoinType() && !coinGrid[x][y + 1].getVisited()) {
                checkNeighbour(x, y + 1);
            }
        }
        if (x + 1 < coinGrid.length) { //call Down
            if (coinGrid[x][y].getCoinType() == coinGrid[x + 1][y].getCoinType() && !coinGrid[x + 1][y].getVisited()) {
                checkNeighbour(x + 1, y);
            }
        }
    }


    @Override
    public String toString() {
        String result = "";

        for (int i = 0; i < coinGrid.length; i++) {
            for (int j = 0; j < coinGrid[i].length; j++) {
                result += coinGrid[i][j].toString();
            }
            result += "\n";
        }

        return result;
    }
}

