package org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Coins.Coin;

public class Player implements KeyboardHandler {

    private GridPosition pos;
    private Grid grid;
    private Coin coin;
    private Keyboard k;
    private boolean pulling;
    private boolean pushing;
    private boolean down;
    private boolean left;
    private boolean right;
    private boolean restart;


    public Player(GridPosition pos, Grid grid) {

        k = new Keyboard(this);

        this.pos = pos;
        pos.setNewImage("resources/images/lisa.png");
        pos.show();
        this.grid = grid;
        coin = new Coin(Coin.CoinType.NULLCOIN, grid.makeGridPosition(this.pos.getCol(), this.pos.getRow() - 1));

        int[] keys = {
                KeyboardEvent.KEY_A,
                KeyboardEvent.KEY_S,
                KeyboardEvent.KEY_D,
                KeyboardEvent.KEY_P,
                KeyboardEvent.KEY_O,
                KeyboardEvent.KEY_R,
                KeyboardEvent.KEY_SPACE
        };

        for (int key : keys) {
            setKeybindPressed(key);
            setKeybindReleased(key);
        }

    }


    public void move() {
        if (left) {
            pos.moveLeft();
            coin.getCoinPos().moveLeft();
            left = false;
        }
        if (right) {
            pos.moveRight();
            coin.getCoinPos().moveRight();
            right = false;
        }
    }


    public int getPosition() {
        return pos.getCol();
    }

    /*
    public Color getImage() {
        return pos.getImage();
    }
    */

    public boolean isMoving() {
        return left ^ right;
    }

    public Coin.CoinType getPlayerType() {
        return coin.getCoinType();
    }

    public void setPlayerType(Coin.CoinType type) {
        this.coin.setType(type);
       /* if (type != Coin.CoinType.NULLCOIN) {
            pos.setColor(this.coin.getCoinType().getColor());
            return;
        }
        pos.setColor(Color.BLACK);*/

    }

    @Override
    public void keyPressed(KeyboardEvent e) {

        switch (e.getKey()) {
            case KeyboardEvent.KEY_S:
                down = true;
                break;
            case KeyboardEvent.KEY_A:
                left = true;
                break;
            case KeyboardEvent.KEY_D:
                right = true;
                break;
            case KeyboardEvent.KEY_P:
                pulling = true;
                break;
            case KeyboardEvent.KEY_O:
                pushing = true;
            case KeyboardEvent.KEY_R:
                restart = true;
                break;


        }
    }

    @Override
    public void keyReleased(KeyboardEvent e) {

        switch (e.getKey()) {
            case KeyboardEvent.KEY_S:
                down = false;
                break;
            case KeyboardEvent.KEY_A:
                left = false;
                break;
            case KeyboardEvent.KEY_D:
                right = false;
                break;
            case KeyboardEvent.KEY_P:
                pulling = false;
                break;
            case KeyboardEvent.KEY_O:
                pushing = false;
                break;
            case KeyboardEvent.KEY_R:
                restart = false;
                break;


        }

    }

    public boolean isPushing() {
        return pushing;
    }

    public void stopPushing() {
        pushing = false;
    }

    public boolean isPulling() {
        return pulling;
    }

    public void stopPulling() {
        pulling = false;
    }

    public void remove(){
        pos.remove();
    }

    public boolean isDown() {
        return down;
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isRestart(){
        return restart;
    }

    private void setKeybindPressed(int event) {
        KeyboardEvent newBind = new KeyboardEvent();
        newBind.setKey(event);
        newBind.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(newBind);
    }

    private void setKeybindReleased(int event) {
        KeyboardEvent newBind = new KeyboardEvent();
        newBind.setKey(event);
        newBind.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        k.addEventListener(newBind);
    }

}
