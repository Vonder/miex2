package org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringrays.moneyidolexchanger2.CoinGrid;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Coins.Coin;

public class Grid {

    private int padding = 10;
    private int cellSize = 50;
    private int cols;
    private int rows;

    private CoinGrid coinGrid;

    private Picture background;

    /*
    private int[][] pattern = {
            {2, 1, 2, 1, 2},
            {1, 2, 1, 2, 1},
    };
    */

    public Grid(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;

        background = new Picture(10,10, "resources/images/field/gamebg.png");

        background.draw();

    }


    public void init() {
        background.draw();
        coinGrid = new CoinGrid(cols, rows);
        coinGrid.fillCoinGrid(this);
        coinGrid.spawnCoins();
        newRow();
        newRow();
    }

    public void newRow() {
        coinGrid.descendCoins();
        coinGrid.spawnCoins();
    }


    public Coin.CoinType pullCoins(int col) {

        return coinGrid.getTypeOfFirstCoin(col);

    }


    public int howMany(int col, Coin.CoinType type) {

        return coinGrid.getAllCoinsOfType(col, type);

    }

    public boolean gameIsOver(){
        return coinGrid.checkGameOver();
    }



    public void placeCoins(int col, Coin.CoinType type, int quantity) {
        coinGrid.pushCoinsOnCol(col, type, quantity);
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public int getWidth() {
        return background.getWidth();
    }

    public int getHeight() {
        return background.getHeight();
    }

    public int getX() {
        return background.getX();
    }

    public int getY() {
        return background.getY();
    }

    public int getCellSize() {
        return cellSize;
    }

    public GridPosition makeGridPosition(int col, int row) {
        return new GridPosition(col, row, this);
    }

    public int rowToY(int row) {
        return row * cellSize;
    }

    public int columnToX(int column) {
        return column * cellSize;
    }
}

