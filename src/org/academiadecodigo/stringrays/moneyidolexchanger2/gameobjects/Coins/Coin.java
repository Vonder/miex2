package org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Coins;

import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Grid;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.GridPosition;

public class Coin {

    private CoinType type;
    private GridPosition pos;

    private boolean visited;

    public Coin(CoinType type, GridPosition pos) {
        this.pos = pos;
        this.type = type;
        pos.setNewImage(CoinType.ONE.getImage());
        pos.show();
        //pos.setColor(type.getImage());
    }

    public Coin(CoinType type) {
        this.type = type;
    }

    public GridPosition getCoinPos() {
        return pos;
    }

    public CoinType getCoinType() {
        return type;
    }


    public static CoinType getNextValue(CoinType type){
       return Coin.CoinType.values()[type.ordinal()+1];
    }



    public void setType(CoinType type) {
        this.type = type;
        pos.setNewImage(type.getImage());
    }

    public void setVisited() {
        visited = true;
    }

    public void setUnvisited() {
        visited = false;
    }

    public boolean getVisited() {
        return visited;
    }

    @Override
    public String toString() {
        return pos.toString() + ":" + type + "    ";
    }

    public enum CoinType {

        ONE("resources/images/coins/coin-1.png", 5),
        FIVE("resources/images/coins/coin-2.png", 2),
        TEN("resources/images/coins/coin-3.png", 5),
        FIFTY("resources/images/coins/coin-4.png", 2),
        HUNDRED("resources/images/coins/coin-5.png", 5),
        NULLCOIN("resources/images/empty.png", 1);

        private String image;
        private int value;

        CoinType(String image, int type) {
            this.image = image;
            this.value = type;
        }

        public String getImage() {
            return image;
        }

        public int getDivider() {
            return value;
        }
    }
}
