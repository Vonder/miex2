package org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GridPosition {

    private Picture image;
    private Grid grid;


    /**
     * Simple graphics position constructor
     *
     * @param col  position column
     * @param row  position row
     * @param grid Simple graphics grid
     */
    public GridPosition(int col, int row, Grid grid) {
        this.grid = grid;
        image = new Picture(grid.columnToX(col) + grid.getX(), grid.rowToY(row) + grid.getY(), "resources/images/empty.png");
        show();
    }

    public void show() {
        image.draw();
    }

    public void moveLeft() {
        image.delete();
        if (image.getX() >= grid.getX() + grid.getCellSize()) {
            image.translate(-1 * grid.getCellSize(), 0);
        }
        show();
    }

    public void moveRight() {
        image.delete();
        if (image.getX() < grid.getWidth() - grid.getCellSize()) {
            image.translate(grid.getCellSize(), 0);
        }
        show();
    }

    public void setNewImage(String path) {
        image.load(path);
    }

    public void remove(){
        image.delete();
    }

    public int getCol() {
        return image.getX() / grid.getCellSize();
    }

    public int getRow() {
        return image.getY() / grid.getCellSize();
    }

    /*
    public void setColor(Color color){

        image.setColor(color);
        show();
    }

    public Color getImage() {
        return image.getImage();
    }
    */

    @Override
    public String toString() {
        return "col:" + getCol() + "| row:" + getRow();
    }
}
