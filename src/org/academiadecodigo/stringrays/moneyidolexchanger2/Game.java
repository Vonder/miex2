package org.academiadecodigo.stringrays.moneyidolexchanger2;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Coins.Coin;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Grid;
import org.academiadecodigo.stringrays.moneyidolexchanger2.gameobjects.Player;

public class Game {

    private Player player;
    private Grid gameGrid;
    private int playerPocket;
    private int gameTime;


    private GameOver gameOver = new GameOver(75, 200);

    public void init() {

        gameGrid = new Grid(7, 12);

        player = new Player(gameGrid.makeGridPosition(0, 11), gameGrid);
        playerPocket = 0;

        gameGrid.init();
        System.out.println(Coin.CoinType.values()[Coin.CoinType.ONE.ordinal() + 1]);
    }




    public void start() throws InterruptedException {

        while (true) {


            if(gameGrid.gameIsOver()){
                gameOver.imageShow();
                System.out.println("Game is Over");

                while(!player.isRestart()){

                    Thread.sleep(100);
                }

                gameOver.picDelete();
                System.out.println("Type X to restart");
                player.remove();
                init();

            }
            Thread.sleep(60);
            gameTime++;

            if (gameTime >= 100) {
                gameGrid.newRow();
                gameTime = 0;
            }


            if (player.isMoving()) {
                player.move();
            }
            if (player.isDown()) {
                gameGrid.newRow();
            }

            if (player.isPulling()) {

                Coin.CoinType playerCoin = player.getPlayerType();

                if (playerCoin == Coin.CoinType.NULLCOIN) {
                    playerCoin = gameGrid.pullCoins(player.getPosition());
                }

                playerPocket += gameGrid.howMany(player.getPosition(), playerCoin);
                player.setPlayerType(playerCoin);
                player.stopPulling();

            }

            if (player.isPushing()) {

                if (playerPocket > 0) {

                    gameGrid.placeCoins(player.getPosition(), player.getPlayerType(), playerPocket);
                    playerPocket = 0;
                    player.setPlayerType(Coin.CoinType.NULLCOIN);
                    player.stopPushing();

                }

            }

        }


    }






    private class GameOver{

        private Rectangle rectangle;
        private Picture imageGameOver;
        private Picture anime;
        private int cols;
        private int rows;


        private String[] picPath = {
                "resources/images/gameover3.png"

    };


        public GameOver(int cols, int rows){

            this.cols = cols;
            this.rows = rows;
        }

        public void picDelete(){
            imageGameOver.delete();

        }

        public void imageDraw(){
            this.imageGameOver.draw();
        }

        public void imageShow(){


            imageGameOver = new Picture(10, 10, picPath[0]);
            imageGameOver.draw();





        }
        public void imageShow(int cols, int rows){

            imageGameOver = new Picture(cols, rows, picPath[2]);
            imageGameOver.draw();

        }
    }

}
